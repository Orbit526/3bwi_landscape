package at.peter.landscape;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class Landscape extends BasicGame {

	private int rectX, rectY, circleX, circleY, ovalX, ovalY;
	private int degreeX, degreeY;
	private boolean circleRight = true;
	private boolean circleLeft = false;
	private boolean rectUp = false;
	private boolean rectDown = true;
	private boolean rectLeft = false;
	private boolean rectRight = false;
	
	public Landscape() {
		super("Landscape");
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// TODO Auto-generated method stub
		graphics.drawRect(this.rectX, this.rectY, 100, 100);
		graphics.drawOval(this.circleX, this.circleY, 100, 100);
		graphics.drawOval(this.ovalX, this.ovalY, 50, 100);
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
		this.circleX = 100;
		this.circleY = 250;
		
		this.ovalX = 400;
		this.ovalY = 250;
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// TODO Auto-generated method stub
		
		//Kreis hin und her
		if(this.circleX == 600)
		{
			this.circleLeft = true;
			this.circleRight = false;
		}
		else if(this.circleX == 50)
		{
			this.circleLeft = false;
			this.circleRight = true;
		}
		
		if(this.circleLeft == true)
		{
			this.circleX--;
		}
		else if(this.circleRight == true)
		{
			this.circleX++;
		}
		
		//Oval oben nach unten
		
		this.ovalY ++;
		
		if(this.ovalY == 600)
		{
			this.ovalY = 0;
		}
		
		//Rechteck im Kreis laufen lassen
		
		//(int)(startposition + radius * Math.sin(this.degreeX));
		this.rectX = (int)(200 + 200 * Math.sin(this.degreeX++));
		this.rectY = (int)(200 + 200 * Math.cos(this.degreeY++));
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Landscape());
			container.setDisplayMode(800, 600, false);
			container.setTargetFrameRate(400);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
